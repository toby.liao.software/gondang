import os, logging
from datetime import date
from .downloadCREM import download
from lib.save2DB import MySQLHelper
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class CREMConnection(metaclass = MetaSingleton):
    def __init__(self):
        self.sqlHelper = MySQLHelper('CREM')
        self.create_date = ''
        self.site = ''
        self.file_content = []
        
    @staticmethod
    def groupby(objs):
        # Demand(Unconstrained)
        # ProjectedOH(Unconstrained)
        returnDict = {} 
        with open(objs) as obj:
            for line in obj:
                site,sku,desc_site,descrip,v = line.strip().split("||", maxsplit=4)
                values= [values for values in v.split("||", maxsplit=30)]
                if sku not in returnDict: 
                    returnDict[sku] = [[],[]]
                if 'Demand(Unconstrained)' in descrip: returnDict[sku][0] = [int(value) for value in values[:-1]]
                elif 'ProjectedOH(Unconstrained)' in descrip:returnDict[sku][1] = [int(value) for value in values[:-1]]
            return returnDict

    def calculate(self, filename):
        if ('-Detail-' in filename.split(".")[1]):
            self.site = filename.split('-')[-1].split('.')[0]
            self.create_date = filename.split('-')[0].split('/')[-1][:8]
            today_date = eval(global_config.TODAY_DATE).strftime('%Y-%m-%d')
            groupby_result = self.groupby(filename)
            separator = ','
            self.file_content = []
            for key, value in groupby_result.items():
                self.file_content.append({
                    "DATE":self.create_date[:4]+'-'+self.create_date[4:6]+'-'+self.create_date[6:],
                    "SKU": key[:-3],
                    "VERSION": key[-2:],
                    "SITE": self.site,
                    "DEMAND_UNCONSTRAINED": separator.join([str(i) for i in value[0]]),
                    "ON_HAND": value[0][0]+value[1][0],
                    "IMPORT_DATE": today_date
                })
        else: logging.warning('{}. File not support.'.format(filename))

    def update(self):
        self.file_list = download()
        for filename in self.file_list:
            try:
                filename = './cron_job/CREM/'+filename 
                self.calculate(filename)
                self.sqlHelper.delete('DATE=STR_TO_DATE("{}", "%Y%m%d") AND SITE = "{}"'.format(self.create_date, self.site))
                self.sqlHelper.insert(self.file_content)
                logging.info("CREM data updated({} rows updated).".format(len(self.file_content)))
            except: logging.warning('{} is not loaded by now. Pls wait for the update or contact the OT.'.format(filename))
        self.sqlHelper.close()
        