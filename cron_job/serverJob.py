import pyodbc, logging
import pandas as pd
from datetime import date, timedelta
from lib.save2DB import MySQLHelper
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class DailyJobConnection(metaclass = MetaSingleton):
    def __init__(self):
        self.sqlQueryHelper = MySQLHelper('CREM')
        self.sqlHelper = MySQLHelper('HISTORICAL_DEMAND')
        self.create_date = ''
        self.keys = {}
        self.file_content = []

    @staticmethod
    def get_packout(m_no, date, factory):
        site_dict = {
            'TCGA':'TCEB', 
            'ACGE':'ACEE',
            'CZ05':'FCZ',
            'MCGB':'950' }
        cnxn = pyodbc.connect('DRIVER={}; SERVER={}; PORT={}; DATABASE={}; UID={}; PWD={}'.format(
            global_config.PACKOUT_DRIVER, global_config.PACKOUT_SERVER, global_config.PACKOUT_SERVER_PORT, 
            global_config.PACKOUT_DATABASE, global_config.PACKOUT_UID, global_config.PACKOUT_PASSWD
        ))
        ROP = pd.read_sql_query('''
                SELECT M_QTY FROM Material7Columns 
                WHERE M_NO=\'%s\' AND M_DATE=\'%s\' AND Factory=\'%s\'
            ''' %(m_no, date, site_dict[factory]),cnxn)
        if len(ROP.M_QTY): return int(ROP.M_QTY.values[0])
        else: return 0

    def get_amount(self, key, tmpDict):
        # Will only be the Sunday
        CUR_DATE = key[0]
        # (DATE, SKU, VERSION, SITE): DEMAND_UNCONSTRAINED -> string
        LAST_SUNDAY = CUR_DATE - timedelta(days=7)
        try:
            LAST_SUNDAY_KEY = (LAST_SUNDAY, *key[1:])
            packetout = self.get_packout('{}-{}'.format(
                LAST_SUNDAY_KEY[1], LAST_SUNDAY_KEY[2]), 
                LAST_SUNDAY_KEY[0].strftime('%Y-%m-%d'), 
                LAST_SUNDAY_KEY[3])
            # 上週 DF site需求總和
            last_week_df_sum_total = [sum([int(x) for x in tmpDict[LAST_SUNDAY_KEY].split(',')[:8]]), packetout]
        except:
            last_week_df_sum_total = [0,0]
            logging.warning('No packout value on {} in database({}).'.format(LAST_SUNDAY.strftime('%Y-%m-%d'), "{}-{}".format(key[1], key[2])))
        

        last_one_week_result = []
        packout_list = []
        for idx in range(7):
            FOLLOWING_DATE = CUR_DATE - timedelta(days=idx)
            KEY_UPDATE = (FOLLOWING_DATE, key[1], key[2], key[3])
            try: last_one_week_result.append(sum([int(x) for x in tmpDict[KEY_UPDATE].split(',')[:idx+1]]))
            except: pass
            packout_list.append(self.get_packout('{}-{}'.format(KEY_UPDATE[1], KEY_UPDATE[2]), KEY_UPDATE[0].strftime('%Y-%m-%d'), KEY_UPDATE[3]))
        
        
        WEEK_TABLE = []
        if len(last_one_week_result) < 4:
            logging.warning('Weekly Daily Value is lower than 4 in {}.({}, {}).'.format(
                LAST_SUNDAY.strftime('%Y-%m-%d'),
                "{}-{}".format(key[1], key[2]), 
                key[3]))
            return False
        else:
            avg = (sum(last_one_week_result)/len(last_one_week_result))
            last_one_week_result = [[avg, po] for po in packout_list]
            WEEK_TABLE = [*last_one_week_result, last_week_df_sum_total][::-1]
        
        # 需求增減量
        week_request = 0
        for idx, (df_sum, po) in enumerate(WEEK_TABLE):
            if idx == 0: pass
            else: 
                last_df_sum, last_packetout = WEEK_TABLE[idx-1]
                week_request += (df_sum - last_df_sum + last_packetout)
        
        return week_request + last_week_df_sum_total[0]
        
    def calculate(self):
        tmpDict = {}
        # retrieve 2 week value
        # # debug
        # today_date = date(2020, 11, 6)
        today_date = eval(global_config.TODAY_DATE)
        current_date = today_date.strftime('%Y-%m-%d')
        filter_date = (today_date - timedelta(days=14)).strftime('%Y-%m-%d')
        search_result = self.sqlQueryHelper.select(
            'DATE BETWEEN STR_TO_DATE("{}", "%Y-%m-%d") AND STR_TO_DATE("{}", "%Y-%m-%d")'.format(filter_date, current_date))
        
        # for rowproxy in self.sqlQueryHelper.getall():
        for rowproxy in search_result:
            DATE, SKU, VERSION, SITE, DEMAND_UNCONSTRAINED, _, _ = rowproxy
            tmpDict[(DATE, SKU, VERSION, SITE)] = DEMAND_UNCONSTRAINED
        
        for self.keys, _ in tmpDict.items():
            weekday = (self.keys[0]).weekday()
            if weekday == 6 and self.keys[0].date() > (today_date - timedelta(days=7)): # sunday
                self.create_date = (self.keys[0] - timedelta(days=7)).strftime('%Y-%m-%d')
                amount = self.get_amount(self.keys, tmpDict)
                if amount:
                    self.file_content.append({
                        "DATE": self.create_date,
                        "SKU": self.keys[1],
                        "VERSION": self.keys[2],
                        "SITE": self.keys[3], 
                        "AMOUNT": amount,
                        "IMPORT_DATE": current_date
                    })
                    self.update()
                    self.file_content = []
    
    def update(self):
        try:
            self.sqlHelper.delete('DATE=STR_TO_DATE("{}", "%Y-%m-%d") AND SKU = "{}" AND VERSION = "{}" AND SITE = "{}"'
                .format(self.create_date, self.keys[1], self.keys[2], self.keys[3]))
            self.sqlHelper.insert(self.file_content)
            logging.info("HISTORICAL_DEMAND data updated({} rows updated).".format(len(self.file_content)))
        except: 
            logging.warning('No data to update by now. Pls wait for the update or contact the OT.')
