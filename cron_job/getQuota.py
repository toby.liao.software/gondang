import ftplib, os
import logging
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class Quota(metaclass = MetaSingleton):
    def __init__(self):
        self.server = ftplib.FTP()
        self.save_dir = './cron_job/quota'
    
    def connect(self):
        try:
            self.server.connect(global_config.FTP_SERVER)
            self.server.login(
                global_config.QUOTA_FTP_ACCOUNT,
                global_config.QUOTA_FTP_PASSWD)
            self.server.cwd(global_config.QUOTA_FTP_DIR)
        except TimeoutError: 
            logging.warning("The FTP server connection timeout. Please contact the IT. (Quota)")
            logging.warning('Program disrupt, retrying in 10 seconds...')
            exit()

    def get_quota_flist(self, name):
        tmp = []
        results = []
        self.server.dir(tmp.append)
        for line in [file.split(" ")[-1] for file in tmp if file.split(" ")[-1] not in os.listdir(self.save_dir)]:
            if name in line:
                results.append(line)
        return results

    def get_quota(self):
        self.connect()
        filename_re = 'Quotadata'
        
        for f in self.get_quota_flist(filename_re):
            if os.path.isfile(f)==False:
                self.server.retrbinary("RETR "+f, open(f,'wb').write)
                logging.info('Downloading file {} from FTP server.'.format(f))
                if filename_re in f: os.system("mv *{}* {}".format(filename_re, self.save_dir))
