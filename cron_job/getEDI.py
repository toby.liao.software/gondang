import ftplib, logging
import pandas as pd 
from datetime import timedelta, date
from lib.save2DB import MySQLHelper
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class EDIConnection(metaclass = MetaSingleton):
    def __init__(self):
        self.sqlHelper = MySQLHelper('EDI')
        self.create_date = ''
        self.file_content = []
    
    def connect(self):
        try:
            self.server = ftplib.FTP()
            self.server.connect(global_config.FTP_SERVER)
            self.server.login(global_config.EDI_FTP_ACCOUNT, global_config.EDI_FTP_PASSWD)
            self.server.cwd(global_config.EDI_FTP_DIR)
        except TimeoutError: 
            logging.warn("The FTP server is down. Please contact the IT.")
            logging.warning('Program disrupt, retrying in 10 seconds...')
            exit()


    def calculate(self):
        self.file_content = []
        ftpFileList=[]
        self.connect()
        self.server.dir(ftpFileList.append)
        self.server.quit()
        today_date = eval(global_config.TODAY_DATE)
        site_dict = {
            'FCZ': 'CZ05',
            'FOC': 'ACGE',
            'FTX': 'TCGA',
            'FJZ': 'MCGB',
        }
        # 歸整至上週一時間，因為會重複的掃描
        today_date = (today_date-timedelta(days=today_date.weekday()))
        for file in ftpFileList:
            if ("EDI830_Smart_Ops_ROP_report_"+today_date.strftime("%Y%m%d")) in file.split(".")[0]:
                weekday = today_date.weekday()
                if weekday==0:
                    df = pd.read_excel('ftp://ROPuser:ROPuser@10.132.48.74/SmartOpsROP_report/%s' %file.split(" ")[-1],skiprows=[0])
                    for idx, row in df.iterrows():                    
                        separator = ','
                        self.create_date = (today_date- timedelta(days=1)).strftime("%Y-%m-%d")
                        self.file_content.append({
                            "DATE": self.create_date,
                            "SKU": row['M_NO'],
                            "VERSION": row['M_No_Version'][-2:],
                            "SITE": site_dict[row['plant']], 
                            "EDI_VALUE": separator.join(
                                [str(i) for i in df.iloc[idx,4:].values]),
                            "IMPORT_DATE": eval(global_config.TODAY_DATE).strftime('%Y-%m-%d')
                        })
   
    def update(self):
        try:
            self.calculate()
            self.sqlHelper.delete('DATE=STR_TO_DATE("{}", "%Y-%m-%d")'.format(self.create_date))
            self.sqlHelper.insert(self.file_content)
            self.sqlHelper.close()
            logging.info("EDI data updated({} rows updated).".format(len(self.file_content)))
        except: logging.warning('No data to update by now. Pls wait for the update or contact the OT.')
