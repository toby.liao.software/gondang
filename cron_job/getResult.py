import os, logging
import pandas as pd
import ftplib
from datetime import date, datetime, timedelta
from .getQuota import Quota
from lib.save2DB import MySQLHelper
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class PredictResult(metaclass = MetaSingleton):
    def __init__(self):
        self.file_content = []
        self.keys = []
        self.create_date = ''
        self.sqlHelper = MySQLHelper('SYSTEM_RESULT')
        self.quota = Quota()
        
    def readCSV(self):
        self.quota.get_quota()
        with open('./predict_result.csv', 'r') as predictRows:
            next(predictRows)
            for predictRow in predictRows:
                token = predictRow.split(',')
                readROP_result = self.readROP(token[1], token[0], token[2])
                quotaDict = self.getQuota(token[0])
                try : quota = quotaDict[(token[0], token[1], token[2])]
                except: quota = 100
                self.file_content.append({
                    "DATE": token[0],
                    "SKU": token[2][:-3],
                    "VERSION": token[2][-2:],
                    "SITE": token[1], 
                    "RESULT_W1": float(token[6]),
                    "RESULT_W2": float(token[9]),
                    "RESULT_W3": float(token[12]),
                    "CT2R": str(readROP_result[0]),
                    "SM_CT2R": str(readROP_result[2]),
                    "DDLT": str(readROP_result[1]),
                    "SM_DDLT": str(readROP_result[3]),
                    "QUOTA": quota,
                    "INTRANSIT": 'NA',
                    "IMPORT_DATE": eval(global_config.TODAY_DATE).strftime('%Y-%m-%d')
                })
                self.create_date = token[0]
                self.keys = [token[2][:-3], token[2][-2:], token[1]]
                self.update()
                self.file_content = []
                
    @staticmethod
    def readROP(site, pred_date, sku):
        pred_date = (datetime.strptime(pred_date, "%Y-%m-%d")+timedelta(days=1)).strftime("%Y%m%d")
        dfsite_mapping = {
            'MCGB': 'MCGA',
            'TCGA': 'FTX',
            'ACGE': 'CDF',
            'CZ05': 'FCZ'}
        dfsite_filename = ''
        sm_filename = ''
        
        for f in os.listdir('./model/rop/'):
            if f.startswith(dfsite_mapping[site]) and pred_date in f:
                dfsite_filename = './model/rop/'+f
            if f.startswith('SM') and pred_date in f:
                sm_filename = './model/rop/'+f

        dfsite = pd.read_excel(dfsite_filename, sheet_name = 'DDLT_Report')
        sm = pd.read_excel(sm_filename, sheet_name = 'DDLT_Report')
        def checkNULL(arr_vals):
            if list(arr_vals) == []: return 'NA'
            else: return list(arr_vals)[0]
        return [
            checkNULL(dfsite[dfsite['PN'] == sku].CT2R.values),
            checkNULL(dfsite[dfsite['PN'] == sku].DDLT.values),
            checkNULL(sm[sm['PN'] == sku].CT2R.values),
            checkNULL(sm[sm['PN'] == sku].DDLT.values)
        ]
        
    @staticmethod
    def getQuota(pred_date):
        # pred_date will be on Sunday
        pred_date = (datetime.strptime(pred_date, "%Y-%m-%d"))
        # quota data imported in Friday
        quotaFile = ''
        quotaPath = './cron_job/quota/'
        
        if pred_date.weekday() < 4: quotaDate = pred_date - timedelta(days=(pred_date.weekday()+3))
        else: quotaDate = pred_date - timedelta(days=(pred_date.weekday()-4))
        for current_file in os.listdir(quotaPath):
            if quotaDate.strftime('%Y%m%d') in current_file: 
                quotaFile = current_file
                break

        return_dict = {}
        with open(quotaPath+quotaFile) as read_file:
            for line in read_file:
                token = line.strip().split("||")
                sku = token[0]
                site = token[1]
                made_in_LH = token[10]
                quota = int(token[12])
                if made_in_LH != '': return_dict[(site, sku)] = quota
                else: return_dict[(pred_date.strftime('%Y-%m-%d'), site, sku)] = 0
        return return_dict

    def update(self):
        try:
            self.sqlHelper.delete('DATE=STR_TO_DATE("{}", "%Y-%m-%d") AND SKU = "{}" AND VERSION = "{}" AND SITE = "{}"'
                .format(self.create_date, self.keys[0], self.keys[1], self.keys[2]))
            self.sqlHelper.insert(self.file_content)
            logging.info("SYSTEM_RESULT data updated({} rows updated).".format(len(self.file_content)))
        except: 
            logging.warning('No data to up date by now. Pls wait for the update or contact the OT.')