import os, ftplib, logging
from datetime import datetime, date
from .logger import initialize_logger
from lib.config import global_config

initialize_logger('./log', 'logger_{}'.format(datetime.now()))

def connect():
    try:
        server = ftplib.FTP()
        server.connect(global_config.FTP_SERVER)
        server.login(
            global_config.CREM_FTP_ACCOUNT,
            global_config.CREM_FTP_PASSWD)
        return server
    except TimeoutError: 
        logging.warning('The FTP server is down. Please contact the OT.')
        logging.warning('Program disrupt, retrying in 10 seconds...')
        exit()

def get_crem_flist(name):
    a=[]
    server = connect()
    server.dir(a.append)
    server.quit()
    file_list = []
    for line in a :
        if name in line.split(" ")[-1]:
            file_list.append(line.split(" ")[-1])
    return file_list

def download():
    detail = [file for file in get_crem_flist('Detail') if file not in os.listdir('./cron_job/CREM')]
    header = [file for file in get_crem_flist('Header') if file not in os.listdir('./cron_job/CREM_header')]
    current_site_update = [file.split('-')[-1].split('.')[0] for file in detail if eval(global_config.TODAY_DATE).strftime('%Y%m%d') in file]
    sites = ['TCGA', 'MCGB', 'ACGE', 'CZ05']
    not_exist = [site for site in sites if site not in current_site_update]
    for site in not_exist: logging.warning("Site {} still not yet upload the CREM report.".format(site))
    
    files = [*detail, *header]
    ftp = connect()
    

    for f in files:
        if os.path.isfile(f)==False:
            ftp.retrbinary("RETR "+f, open(f,'wb').write)
            logging.info('Downloading file {} from FTP server.'.format(f))
            if 'Detail-' in f: os.system("mv *Detail-* ./cron_job/CREM")
            elif 'Header-' in f: os.system("mv *Header-* ./cron_job/CREM_header")
    
    ftp.quit()
    return detail
    
