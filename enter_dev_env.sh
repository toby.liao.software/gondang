#!/bin/bash -e
CONTAINER="${USER}_gondang_vm"
START_SHELL="sh"
WORKSPACE=${WORKSPACE:-$PWD}
IMAGE=gondang/latest:latest

echo $WORKSPACE
# test if the container is running
HASH=`docker ps -q -f name=$CONTAINER`
 # test if the container is stopped
HASH_STOPPED=`docker ps -qa -f name=$CONTAINER`

if [ -n "$HASH" ];then
    echo "founding existing running container $CONTAINER, proceeed to exec another shell"
    docker exec -it $HASH $START_SHELL
elif [ -n "$HASH_STOPPED" ];then
    echo "founding existing stopped container $CONTAINER, proceeed to start"
    docker start --attach -i $HASH_STOPPED
else
    echo "existing container not found, createing a new one, named $CONTAINER"
    docker run -it --rm \
        --name gondang \
        -v ~/gondang_data/CREM:/GonDang/cron_job/CREM \
        -v ~/gondang_data/log:/GonDang/log \
        -v ~/gondang_data/CREM_header:/GonDang/cron_job/CREM_header \
        -v ~/gondang_data/rop:/GonDang/model/rop \
        --entrypoint $START_SHELL $IMAGE \
        -xec $START_SHELL
fi
echo "see you, use 'docker rm $CONTAINER' to kill the vm if you want a fresh env next time"
