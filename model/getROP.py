import ftplib, os
import logging
from datetime import date
from lib.singletonMeta import MetaSingleton
from lib.config import global_config

class Rop(metaclass = MetaSingleton):
    def __init__(self):
        self.server = ftplib.FTP()
    
    def connect(self):
        try:
            self.server.connect(global_config.FTP_SERVER)
            self.server.login(
                global_config.ROP_FTP_ACCOUNT,
                global_config.ROP_FTP_PASSWD
            )
            self.server.cwd(global_config.ROP_FTP_DIR)
        except TimeoutError: 
            logging.warning("The FTP server connection timeout. Please contact the IT. (ROP)")
            logging.warning('Program disrupt, retrying in 10 seconds...')
            exit()

    def get_rop_flist(self, name):
        tmp = []
        file_dict = {}
        self.server.dir(tmp.append)        
        for line in [file.split(" ")[-1] for file in tmp if file.split(" ")[-1] not in os.listdir('./model/rop')]:
            if name in line:
                date_time = line.split('_')[-1].split('-')[0]
                file_dict[int(date_time)] = line
        results = [file_dict[filename] for filename in file_dict.keys()]
        
        # initialized
        site_cate = {'SM': '','CDF': '','FTX': '','FCZ': '','MCGA': ''}
        for current_file in os.listdir('./model/rop'):    
            for k, _ in site_cate.items():
                if k in current_file:
                    try: prev_date_time = int(site_cate[k].split('_')[-1].split('-')[0])
                    except: prev_date_time = 0
                    date_time = int(current_file.split('_')[-1].split('-')[0])
                    if date_time > prev_date_time: site_cate[k] = current_file                 

        if os.listdir('./model/rop') == [] and results == []: 
            logging.warning("Site {} still not yet upload the ROP report.".format(name.split('_')[0]))
        
        elif eval(global_config.TODAY_DATE).strftime('%Y%m%d') not in site_cate[name.split('_')[0]]:
            if results == [] or eval(global_config.TODAY_DATE).strftime('%Y%m%d') not in results[-1]:
                logging.warning("Site {} still not yet upload the ROP report.".format(name.split('_')[0]))
        return results

    def get_rop(self):
        self.connect()
        site_list = ['SM','CDF','FTX','FCZ','MCGA']
        re = ['{}_Smart_Ops_ROP_report'.format(site) for site in site_list]
        fileList = []
        for type in re:
            for filename in self.get_rop_flist(type):
                fileList.append(filename)
        
        for f in fileList:
            if os.path.isfile(f)==False:
                self.server.retrbinary("RETR "+f, open(f,'wb').write)
                logging.info('Downloading file {} from FTP server.'.format(f))
                if '_Smart_Ops_ROP_report_' in f: os.system("mv *_Smart_Ops_ROP_report_* ./model/rop")
        