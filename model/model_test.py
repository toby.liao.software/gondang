import logging
import joblib
import pandas as pd
from datetime import date, timedelta
from .extract_feature import FeatureExtract
from .train_test_func import CreateTestingSet, modelPrediction
from lib.config import global_config

class TestModel():
    def __init__(self):
        self.MODE = 1 # Training: 0, Testing: 1
        self.FILL_MISS_MODE = 0 # Fill_miss: 0, Regular: 1
        self.WEEK_NUM = 1
        self.testingSet = CreateTestingSet()
        self.fe = FeatureExtract()        

    def main(self):
        trigger = (eval(global_config.TODAY_DATE)-timedelta(days=1)) # input算到哪一天
        results = []
        df_list = ['ACGE','CZ05','TCGA','MCGB']
        col = ['site','sku','date','rate_fill','EDI','Crem_orders']
        for site in df_list:
            df = pd.DataFrame(
                columns = col,
                data = self.fe.feature_extract(
                    trigger-timedelta(days=(trigger.weekday()+1)),
                    trigger, site, self.MODE))
            results.append(df)


        df_test = pd.concat([*results], axis=0,ignore_index = True)
        df_test.to_csv(r'./inter_data/pre_test_data.csv')
        df_test = pd.read_csv(r'./inter_data/pre_test_data.csv', keep_default_na=False)
        data_test = self.testingSet.get_testing_set(df_test, self.WEEK_NUM, 4, trigger, self.FILL_MISS_MODE) 
        
        model = joblib.load('reg_model.pkl')
        result = modelPrediction(data_test, model)
        result.to_csv("./predict_result.csv", index=None)
        logging.info("Finish the prediction.")
