import numpy as np
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.linear_model import LinearRegression

def Ohit(y, X , criterion = 'HDBIC', c1 = 5, c3 = 2.01):
    y_ori = y; X_ori = X
    #數值中心化
    y = scale(y, with_std=False)
    
    nunique = X.apply(pd.Series.nunique)
    cols_to_drop = nunique[nunique == 1].index
    X = X.drop(cols_to_drop, axis=1)
    X_ori = X_ori.drop(cols_to_drop, axis=1)
    X = scale(X, with_std=False)

    #初始化
    (n,p) = np.shape(X)
    y_hat = np.zeros(n)
    ind = []
    trim_ind = []
    HDIC = []
    #設定訊息閥值
    criterion_selector = {
            "HDBIC" : np.log(n),
            "HDAIC" : 2,
            "HDHQ" : c3*np.log(np.log(n))
            }
    w_n = criterion_selector[criterion]

    #演算法
    k = int(np.floor(c1*np.sqrt(n/np.log(p))))
    #變數掃描
    for m in range(k):
        U = y - y_hat
        COR = [np.corrcoef(X[:,i], U)[0,1] for i in range(p)]
        choosed = np.argmax(COR)
        ind.append(choosed)
        reg = LinearRegression().fit(X[:,ind], y)
        y_hat = reg.predict(X[:,ind])
        mse = np.mean( (y - y_hat)**2 )
        HDIC.append(n*np.log(mse) + m*w_n*np.log(p))
    OGA_ind = ind[:(np.argmin(HDIC)+1)]
    #變數篩選
    if len(OGA_ind)>1:
        Trim_ind = []
        base = np.min(HDIC)
        trim_data = X[:,OGA_ind]
        for i in range(len(OGA_ind)):
            reg = LinearRegression().fit(np.delete(trim_data,i,1), y)
            y_hat = reg.predict(np.delete(trim_data,i,1))
            mse = np.mean( (y - y_hat)**2 )
            if n*np.log(mse)+(len(OGA_ind)-1)*w_n*np.log(p) > base:
                Trim_ind.append(OGA_ind[i])
    else:
        Trim_ind = OGA_ind

    #配適最終模型
    final_model = LinearRegression().fit(X_ori.iloc[:,Trim_ind], y_ori)
    feature_name = X_ori.columns[Trim_ind].values
    return([final_model, feature_name])