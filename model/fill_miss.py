import pandas as pd
from datetime import datetime, timedelta
from .extract_feature import FeatureExtract


def fill_missing(df, input_time, WEEK_NUM):
    df = df[df['date']!=input_time]
    trigger = datetime.strptime(df['date'][0], '%Y-%m-%d')
    fe = FeatureExtract()
    df_name = ['ACGE','CZ05','TCGA','MCGB']
    results = []

    for site in df_name:
        col = ['site','sku','date','rate_fill','EDI','Crem_orders']
        df = pd.DataFrame(columns=col,
            data=fe.feature_extract(
                trigger-timedelta(WEEK_NUM*7),
                trigger,site,1))
        results.append(df)

    return pd.concat([*results], axis=0, ignore_index = True)
