import logging
import joblib
import pandas as pd
from datetime import date, timedelta
from .extract_feature import FeatureExtract
from .train_test_func import CreateTrainingSet, ModelTraining
from lib.config import global_config

class TrainModel():
    def __init__(self):
        self.MODE = 0 # Training: 0, Testing: 1
        self.trainingSet = CreateTrainingSet()
        self.modelTrain = ModelTraining()
        self.fe = FeatureExtract()        

    def main(self):        
        results = []
        df_list = ['ACGE','CZ05','TCGA','MCGB']
        col = ['site','sku','date','rate_fill','EDI','Crem_orders']
        trigger = (eval(global_config.TODAY_DATE)-timedelta(days=7))#input算到哪一天
        for site in df_list:
            df = pd.DataFrame(
                columns = col,
                data = self.fe.feature_extract(
                    global_config.TRAIN_START_DATE, 
                    trigger, site, self.MODE))
            results.append(df)

        df_train = pd.concat([*results], axis=0,ignore_index = True)
        df_train.to_csv(r'./inter_data/train_data.csv')
        df_train = pd.read_csv(r'./inter_data/train_data.csv', keep_default_na=False)
        data_train = self.trainingSet.get_training_set(df_train, 1, 4) 
        model = self.modelTrain.train(data_train, 4)
        joblib.dump(model, 'reg_model.pkl')
        logging.info(("Done with training model"))
