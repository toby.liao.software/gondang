import logging, joblib
import numpy as np
import pandas as pd
from datetime import timedelta
from sklearn.cluster import KMeans
from .core import Ohit
from .fill_miss import fill_missing

class CreateTrainingSet():
    def __init__(self):
        self.data_train = {}
   
    @staticmethod
    def get_future_past_demand(row, data, f_lag, p_lag):
        date = pd.to_datetime(row['date']).date()
        sku = row['sku']
        site = row['site']
        value = []
        for i in range(f_lag-1):
            try:
                a = data.loc[(data.sku==sku)&(data.site==site)& (data.date == str(date+ timedelta(days=i*7+7)))]['f_demand_1'].values[0]
                value.append(a)
            except: value.append('NA')
        for i in range(p_lag):
            try:
                a = data.loc[(data.sku==sku)&(data.site==site)& (data.date == str(date- timedelta(days=i*7+7)))]['f_demand_1'].values[0]
                value.append(a)
            except: value.append('NA')
        return value
        
    def preprocess(self, data_all):
        logging.info('Create training set:')
        logging.info('Remove data without EDI.')
        self.data_train = data_all[data_all.EDI != 'NA']
        logging.info('1. Remove data without rate_fill.')
        NA_list = self.data_train.groupby(['site','sku']).apply(lambda x: all(x['rate_fill'] == 'NA'))
        index_list = []
        for index, item in self.data_train.iterrows():
            if (item['site'], item['sku']) in list(NA_list.index[NA_list == False]):
                index_list.append(index)
        self.data_train = self.data_train[self.data_train.index.isin(index_list)]
        self.data_train = self.data_train[(self.data_train.rate_fill != 'NA') & (self.data_train.Crem_orders != 'NA')]

        logging.info('2. Processing the EDI column.')
        def process(df, week): return [float(element.split(',')[week]) for element in df['EDI']]
        for i in range(65):
            self.data_train['week%d'%(i+1)] = process(self.data_train, i)
        self.data_train = self.data_train.drop(columns = ['EDI'])

    def calculate(self, data_all, p_lag, f_lag):
        self.preprocess(data_all)

        logging.info('Generating f_demand_1.')
        self.data_train['f_demand_1'] = self.data_train.apply(lambda x: float(x.rate_fill) + float(x.Crem_orders), axis = 1)
        
        logging.info('Create past and future demand.')
        tmp = pd.DataFrame(zip(*self.data_train.apply(self.get_future_past_demand, data = self.data_train, p_lag = p_lag, f_lag = f_lag, axis = 1))).transpose()
        tmp.columns = ['f_demand_%d'%i for i in range(2,f_lag+1)]+['p_demand_%d'%i for i in range(1,p_lag+1)]
        self.data_train = pd.concat([self.data_train.reset_index(drop=True), tmp.reset_index(drop=True)], axis = 1)

        logging.info('Create weekly difference.')
        self.data_train['diff_week1'] = self.data_train.apply(lambda x: float(x['Crem_orders']) - float(x['week1']), axis = 1)
        self.data_train = self.data_train[self.data_train[['p_demand_%d'%i for i in range(1,p_lag+1)]].apply(lambda x: (all(x != 'NA'))&(all(x.isnull() == False)), axis = 1)]
        tmp = pd.DataFrame(zip(*( np.array(self.data_train.loc[:, ['p_demand_%d'%i for i in range(1,p_lag)]]) - np.array(self.data_train.loc[:, ['p_demand_%d'%i for i in range(2,p_lag+1)]])))).transpose()
        tmp.columns = ['diff_demand_%d'%i for i in range(1,p_lag)]
        self.data_train = pd.concat([self.data_train.reset_index(drop=True), tmp.reset_index(drop=True)], axis = 1)
        self.data_train.to_csv(r'./trainingSet.csv')
    
    def get_training_set(self, data_all, p_lag, f_lag):
        self.calculate(data_all, p_lag, f_lag)
        return self.data_train

class ModelTraining():    
    def preprocess(self):
        logging.info('Start clustering the train set.')
        km = KMeans(n_clusters = 4, random_state=0)
        km = km.fit(np.array(self.data_train.loc[:,'diff_week1']).reshape(-1,1))
        self.data_train['clu'] = list(km.labels_)
        cluster_n = list(set(self.data_train.clu))
        label = self.data_train[['f_demand_%d'%i for i in range(1, self.step+1)]]
        self.data_train = self.data_train.drop(columns = ['f_demand_%d'%i for i in range(1, self.step+1)]+['rate_fill'])
        return km, cluster_n, label
    
    def train(self, data_train, step):
        self.data_train = data_train
        self.step = step
        km, cluster_n, label = self.preprocess()
        models = {}
        logging.info("Model training.")
        for forecast_step in range(1, self.step+1):
            y = label['f_demand_%d'%forecast_step]
            X = self.data_train[y != 'NA']
            y = y[ y!= 'NA']
            models[forecast_step] = {}
            for i in cluster_n:
                if len(X[X.clu == i])>1 :
                    fit, name = Ohit.Ohit(
                        y[X.clu==i], 
                        X[X.clu==i].drop(columns = ['site','sku','date','clu']), 
                        criterion = 'HDHQ')
                    models[forecast_step][i] = {'model':fit, 'feature': name}
        models['clu_model'] = km
        return models

class CreateTestingSet():
    def __init__(self):
        self.data_test = {}
        self.last_week = {}

    @staticmethod
    def get_future_past_demand(row, data, f_lag, p_lag):
        date = pd.to_datetime(row['date']).date()
        sku = row['sku']
        site = row['site']
        value = []
        for i in range(f_lag-1):
            try:
                a = data.loc[(data.sku==sku)&(data.site==site)& (data.date == str(date+ timedelta(days=i*7+7)))]['f_demand_1'].values[0]
                value.append(a)
            except: value.append('NA')
        for i in range(p_lag):
            try:
                a = data.loc[(data.sku==sku)&(data.site==site)& (data.date == str(date- timedelta(days=i*7+7)))]['f_demand_1'].values[0]
                value.append(a)
            except: value.append('NA')
        return value
        
    def preprocess(self, data_all):
        logging.info('Testing data processing.')
        logging.info('Remove data without EDI.')
        self.data_test = data_all[data_all.EDI != 'NA']
        logging.info('Remove data without Crem orders.')
        self.data_test = self.data_test[self.data_test.Crem_orders != 'NA']
        logging.info('Processing the EDI column.')
        def process(df, week): return [float(element.split(',')[week]) for element in df['EDI']]
        for i in range(65):
            self.data_test['week%d'%(i+1)] = process(self.data_test, i)
        self.data_test = self.data_test.drop(columns = ['EDI'])
        logging.info('Done with testing data preprocessing.')

    def calculate(self, data_all, p_lag, f_lag , target_date, mode):
        self.TAG = 0
        self.preprocess(data_all)
        logging.info('Generating f_demand_1.')
        def checkNull(data):
            if data.rate_fill != 'NA' and data.Crem_orders != 'NA': 
                return data.rate_fill + data.Crem_orders
            else: return 'NA'
        self.data_test['f_demand_1'] = self.data_test.apply(
            lambda x: checkNull(x), axis = 1)
        logging.info('Finish generating f_demand_1.')

        logging.info('Create the rest past and future demand.')
        if mode == 0: # Fill-miss mode
            logging.info("Mode: FILL-MISS.")
            data_loss = data_all[data_all.rate_fill == 'NA']
            if len(data_loss) > 0: # len > 0
                df_tmp = fill_missing(data_loss, target_date, p_lag)
                data_test = self.get_testing_set(df_tmp, p_lag, f_lag, target_date, 1)
                model = joblib.load('reg_model.pkl')
                self.last_week = modelPrediction(data_test, model) 
                if len(self.last_week)>0:
                    self.TAG = 1
                
        tmp = pd.DataFrame(zip(*self.data_test.apply(self.get_future_past_demand, data = self.data_test, p_lag = p_lag, f_lag = f_lag, axis = 1))).transpose()
        tmp.columns = ['f_demand_%d'%i for i in range(2,f_lag+1)]+['p_demand_%d'%i for i in range(1, p_lag+1)]
        self.data_test = pd.concat([self.data_test.reset_index(drop=True), tmp.reset_index(drop=True)], axis = 1)
        
        if self.TAG == 1:
            # self.data_test.to_csv((r'./inter_data/test_data.csv'))
            # self.last_week.to_csv((r'./inter_data/latestweek_prediction.csv'))
            self.data_test = pd.read_csv(r'./inter_data/test_data.csv', keep_default_na=False)
            self.last_week = pd.read_csv(r'./inter_data/latestweek_prediction.csv', keep_default_na=False)
            for index, row in self.data_test.iterrows():
                site = row.site
                sku = row.sku
                def attempt():
                    if bool(row['p_demand_1']== 'NA') & bool(row['date'] == str(target_date)):
                        try: 
                            value = self.last_week.loc[ bool(self.last_week['site'] == site) & bool(self.last_week['sku'] == sku), 'y_predict_1'].values[0]
                        except: value = 'NA'
                        self.data_test['p_demand_1'][index] = value
                try: attempt()
                except: attempt()
        
        logging.info('Creating weekly difference.')
        self.data_test['diff_week1'] = self.data_test.apply(lambda x: float(x['Crem_orders']) - float(x['week1']), axis = 1)
        self.data_test = self.data_test[self.data_test[['p_demand_%d'%i for i in range(1,p_lag+1)]].apply(lambda x: (all(x != 'NA'))&(all(x.isnull() == False)), axis = 1)]
        
        tmp = pd.DataFrame(zip(*( np.array(self.data_test.loc[:, ['p_demand_%d'%i for i in range(1,p_lag)]]) - np.array(self.data_test.loc[:, ['p_demand_%d'%i for i in range(2,p_lag+1)]])))).transpose()
        tmp.columns = ['diff_demand_%d'%i for i in range(1,p_lag)]
        self.data_test = pd.concat([self.data_test.reset_index(drop=True), tmp.reset_index(drop=True)], axis = 1)
    
    def get_testing_set(self, data_all, p_lag, f_lag, target_date, mode):
        self.calculate(data_all, p_lag, f_lag, target_date, mode)
        return self.data_test

def modelPrediction(data_test, models):
    logging.info('Start clustering...')
    data_test['clu'] = models['clu_model'].predict(np.array(data_test.loc[:,'diff_week1']).reshape(-1,1))
    X = data_test
    result = data_test.loc[:,['date','site','sku','Crem_orders']]
    cluster_n = list(set(data_test.clu))
    logging.info('Start predicting...')
    for forecast_step in range(1,len(models)):
        result['f_demand_%d'%forecast_step] = X['f_demand_%d'%forecast_step]
        result['week%d'%forecast_step] = X['week%d'%forecast_step]
        result['y_predict_%d'%forecast_step] = 0
        for i in cluster_n:
            if sum(X.clu == i) > 0:
                try:
                    beta_model = models[forecast_step][i]['model']; feature = models[forecast_step][i]['feature']
                    result.loc[X.clu == i,'y_predict_%d'%forecast_step] = beta_model.predict(X.loc[X.clu==i, feature]) #OGA
                except:
                    None
    logging.info('Prediction Finished.')
    return result
