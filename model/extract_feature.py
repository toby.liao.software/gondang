import os
import pandas as pd
from datetime import timedelta, date
from lib.save2DB import MySQLHelper

class FeatureExtract():
    def __init__(self):
        self.filename = ''
        self.HisDeQueryHelper = MySQLHelper('HISTORICAL_DEMAND')
        self.EDIQueryHelper = MySQLHelper('EDI')
        self.CREMQueryHelper = MySQLHelper('CREM')

    # General Usage
    def get_orders(self, site, sku, dt):
        select_result = self.HisDeQueryHelper.select(
            'DATE = STR_TO_DATE("{}", "%Y-%m-%d") AND SITE = "{}" AND SKU="{}" AND VERSION="{}"'.format(dt, site, sku[:-3], sku[-2:]))
        if select_result: return list(select_result[0])[4]
        else: return 'NA'

    def get_future_edi(self, site, sku, dt):
        select_result = self.EDIQueryHelper.select(
            'DATE = STR_TO_DATE("{}", "%Y-%m-%d") AND SITE = "{}" AND SKU="{}" AND VERSION="{}"'.format(dt, site, sku[:-3], sku[-2:]))
        if select_result: return list(select_result[0])[4]
        else: return 'NA'

    def get_future_orders(self, site, sku, dt):
        select_result = self.CREMQueryHelper.select(
            'DATE = STR_TO_DATE("{}", "%Y-%m-%d") AND SITE = "{}" AND SKU="{}" AND VERSION="{}"'.format(dt, site, sku[:-3], sku[-2:]))
        if select_result: return sum([int(element) for element in list(select_result[0])[4].split(',')[:8]])
        else: return 'NA'

    def get_sku_list(self, dt):
        # it publish on Monday, so add one more day
        date_str = (dt+timedelta(days=1)).strftime("%Y%m%d")
        for f in os.listdir('./model/rop'):
            if f.startswith('SM') and date_str in f:
                self.filename = './model/rop/'+f
        
        ROP_data = pd.read_excel(self.filename, sheet_name = 'DDLT_Report').loc[:,['PN','BU','CT2R']]
        PN_list_tmp = ROP_data['PN'][(ROP_data.CT2R < 21)].values
        PN_list = []
        for sku in PN_list_tmp:
            head = sku.split('-')[0]
            if head in ['68','800']:
                PN_list.append(sku)
        return PN_list


    # TestingSet-only Section
    # MODE:{TRAINING: 0, TESTING: 1}
    def SKU_update_by_EDI(self, dt, trigger, MODE):
        SKU_from_SM = self.get_sku_list(dt)
        if MODE == 0: return SKU_from_SM
        elif MODE == 1:
            results = self.EDIQueryHelper.select(
                'DATE = STR_TO_DATE("{}", "%Y-%m-%d")'.format(trigger))
            if len(results):
                pn = []
                for result in results:
                    if result[1].split('-')[0] in ['68','800']:
                        pn.append("{}-{}".format(result[1], result[2]))
                return list(set(pn + SKU_from_SM))
            else: return SKU_from_SM
 
    # Export methods
    def feature_extract(self, start_date, trigger, site, MODE):
        row_list= []
        for n in range(0, int((trigger - start_date).days) + 1, 7):
            dt = (start_date + timedelta(days=n))
            PN_list = self.SKU_update_by_EDI(dt, trigger, MODE)
            
            dt = dt.strftime("%Y-%m-%d")
            for sku in PN_list:        
                # 'site','sku','date',
                amount = self.get_orders(site, sku, dt)
                EDI = self.get_future_edi(site, sku, dt)
                Crem_orders = self.get_future_orders(site, sku, dt)
                try: rate_fill = amount-Crem_orders
                except: rate_fill = 'NA'
                this_row = [site, sku, dt, rate_fill, EDI, Crem_orders]
                row_list.append(this_row)
        return(row_list)
