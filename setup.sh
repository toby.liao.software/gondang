mysql -u root -p < ./gondang.sql
docker run --rm \
    --name gondang \
    -v ~/gondang_data/CREM:/GonDang/cron_job/CREM \
    -v ~/gondang_data/log:/GonDang/log \
    -v ~/gondang_data/CREM_header:/GonDang/cron_job/CREM_header \
    -v ~/gondang_data/rop:/GonDang/model/rop \
    gondang/latest:latest 
