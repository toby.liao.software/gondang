import logging
from .config import global_config
from sqlalchemy import create_engine, MetaData, text, select

class MySQLHelper():
    def __init__(self, table_name):
        self.table_name = table_name
        # LOCAL_DEV, STAGING_DEV, PROD_DEV
        self.engine = create_engine("mysql://{}:{}@{}/GonDang".format(
            global_config.LOCAL_DEV_USER,
            global_config.LOCAL_DEV_PASSWD,
            global_config.LOCAL_DEV_SERVER
        ), echo = True)
        self.connect()
        
    def connect(self):
        try:
            self.conn = self.engine.connect()
            self.metadata = MetaData(bind=self.conn, reflect=True)
            self.table = self.metadata.tables[self.table_name]
            # # Solving the issue -> Lost connection to MySQL server during query
            # self.conn.execute('SET GLOBAL max_allowed_packet=67108864')
        except TimeoutError: 
            logging.warning('The Database System is down. Please contact the Hans.')
            logging.warning('Program disrupt, retrying in 10 seconds...')
            exit()
    
    def insert(self, file_content):
        # to make sure in case the document duplicated
        self.conn.execute(self.table.insert(), 
            [dict(t) for t in {tuple(d.items()) for d in file_content}])
    
    def getall(self):
        return self.conn.execute(
            select([self.table])).fetchall()

    def select(self, re):
        return self.conn.execute(
            text("SELECT * FROM {} WHERE {};".format(self.table, re))).fetchall()
          
    def delete(self, re):
        self.conn.execute(text("DELETE FROM {} WHERE {};".format(self.table, re)))

    def close(self):
        self.conn.close()

