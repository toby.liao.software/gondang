FROM python:3.8.6
RUN pip install --upgrade pip
RUN apt-get update -y
RUN apt-get install unixodbc-dev -y
ADD . /GonDang
WORKDIR /GonDang
RUN pip3 install -r requirements.txt
CMD ["sh", "./run.sh"]
