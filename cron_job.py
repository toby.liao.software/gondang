# SET SESSION innodb_log_file_size=268435456;
# SET SESSION max_allowed_packet=134217728;

import os
from cron_job import getCREM, getEDI, serverJob, getResult
from model import model_train, model_test, getROP
from apscheduler.schedulers.blocking import BlockingScheduler

# # cron_job task
# edi = getEDI.EDIConnection()
# crem = getCREM.CREMConnection()
# rop = getROP.Rop()
# sj = serverJob.DailyJobConnection()
# pr = getResult.PredictResult()



# model task
# mtrain = model_train.TrainModel()
# mtest = model_test.TestModel()


# # DEBUG
# edi.update()
# crem.update()
# rop.get_rop()
# sj.calculate()
# mtrain.main()
# mtest.main()
# pr.readCSV()

# # DEV ENV
# # cron_job
# scheduler = BlockingScheduler()
# scheduler.add_job(crem.update, 'cron', hour='17-20')
# scheduler.add_job(edi.update, 'cron', hour=17, minute=30)
# scheduler.add_job(sj.calculate, 'cron', day_of_week='mon-tue', hour=20, minute=20)
# scheduler.add_job(rop.get_rop, 'cron', day_of_week='mon', hour='15-18', minute=40)

# # model
# scheduler.add_job(mtrain.main, 'cron', day_of_week='mon-tue', hour=21)
# scheduler.add_job(mtest.main, 'cron', day_of_week='tue-wed', hour=5)

# print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))
# try:
#     scheduler.start()
# except (KeyboardInterrupt, SystemExit):
#     pass
